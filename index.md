---
name: my first Jekyll website
---

Building Websites with Jekyll and GitLab

## Description
{{ site.description }}
Welcome to {{ page.name }}

Have any questions about what we do? [We'd love to hear from you!](mailto:{{ site.email }}).

More details about the project are available from the [About page](about).